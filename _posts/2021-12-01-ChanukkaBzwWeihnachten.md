---
layout:     einladung
title:      "#beziehungweise: jüdisch und christlich – näher als du denkst"
date:       2021-12-01 18:00:00+01:00
place:      "Ariowitsch-Haus, Hinrichsenstraße 14"
categories: einladung
---

**Chanukka bzw. Weihnachten**

Zu Chanukka wird acht Tage lang jeden Tag eine Kerze mehr am Leuchter angezündet. Licht in der Dunkelheit erinnert an das Licht-Wunder im Jerusalemer Tempel. In der dunklen Jahreszeit feiern Christinnen und Christen die Geburt Jesu, der als Licht in die Welt kommt und entzünden Kerzen im Advent. Parallelen scheinen unverkennbar, aber was haben die beiden Feste tatsächlich miteinander zu tun?
<br>
Den Impuls dazu gibt
*Pfarrerin Simone Berger-Lober*, Leipzig
<br>
Nach dem Impuls wird ausreichend Zeit sein, das Gehörte im Gespräch zu vertiefen.

*Derzeit rechnen wir damit, daß Menschen mit einem aktuellen negativen Test, Impfnachweis oder immunisiert nach einer Genesung ins Ariowitschhaus kommen können.*
<br>
*Darüber hinaus bieten wir die Möglichkeit, über zoom teilzunehmen.*
<br>
Den Zoom-Link zur Veranstaltung finden Sie auf www.ariowitschhaus.de
